# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

require 'byebug'
def guessing_game
  rng = Random.new
  count = 0
  user_guess = 0

  target = rng.rand(99) + 1

  until user_guess == target
    puts("guess a number")
    user_guess = gets.chomp.to_i
    count += 1
    puts("#{user_guess} is too low") if user_guess < target
    puts("#{user_guess} is too high") if user_guess > target
  end

  puts("#{target} was the number and you took #{count} guesses")
end

def file_shuffler
  # filename used here is "7letters.txt"
  puts("give me a file name!")
  filename = gets.chomp

  contents = File.readlines("7letters.txt").shuffle

  File.open("#{filename}-shuffled.txt", 'w') do |f|
    contents.each { |line| f.puts(line) }
  end
end
